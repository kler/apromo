import org.junit.Test;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import static io.restassured.RestAssured.when;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.hasItems;

public class PromotionAPITest {

    public static final String DOMAIN = "http://promo.antoshka.dev.amida.software/api/v1/";

    public static final String LIST_ALL_PROMOTIONS = "promotions";


    public static final String LIMIT = "limit=";
    public static final String PARAMS = "?";
    public static final String SLASH = "/";
    public static final String FROM = "from=";



    String u = DOMAIN + LIST_ALL_PROMOTIONS + "?list=";




    @Test
    public void listAllPromotionsTest() throws MalformedURLException {


//        for (int i = 0; i < 20; i++) {
//            System.out.println(u + i);
//        }

        URL url1 = new URL(DOMAIN +"promotions?limit=20&from=2");
        URL url2 = new URL(DOMAIN +"promotions?limit=20&from=1");
        URL url3 = new URL(DOMAIN +"promotions?limit=20&from=0");
        URL url4 = new URL(DOMAIN +"promotions?limit=20&from=3");


        List<URL> list = new ArrayList<>();
        list.add(url1);
        list.add(url2);
        list.add(url3);
        list.add(url4);
        list.add(url4);

        for(URL url : list) {
            when().
                    get(url)
                    .then()
                    .headers("Content-Type", "application/json")
                    .contentType("application/json")
                    .statusCode(200);
        }



    }

//    @Test
//    public void listProductsOfThePromotionTest() throws MalformedURLException {
//
//        String param = "fugiat-sunt-dolor-velit-nihil-pariatur-quasi-mollitia";
//        URL url = new URL("http://promo.antoshka.dev.amida.software/api/v1/promotions/" + param + "/products?limit=20&from=2");
//
//        when().
//                get(url)
//                .then()
////                .contentType("application/json")
//                .statusCode(200);
//
//    }
}
